


%% predicate to detect empty list
is_empty([]).

%%find_heuristically(S)
%% finds heuristically the schedule with a minimizing cost. Stops after 118 seconds.
find_heuristically(S):- find_heuristically(S, 118).


%%find_heuristically(+S)
%% finds heuristically the schedule with a minimizing cost. Stops after the given seconds.
find_heuristically(S, Time) :- 
    get_time(CurrentTime),
    EndTime is CurrentTime + Time,
    start_loop(EndTime,S).

%% start_loop(+EndTime,-Optimal_schedule)
%% calculates the schedule with a minimizing cost till the given endtime 
start_loop(EndTime,Optimal_schedule):-
    is_valid(S),!,cost(S,C),
    discover_new_optimal(S,C,Optimal_schedule, EndTime).

%% discover_new_optimal(+Schedule,+Cost,-Final_Schedule,+EndTime)
%% keeps searching for the schedule with the minimizing cost till endtime is reached
discover_new_optimal(Schedule,_,Schedule,EndTime):- 
    get_time(Currenttime),
    Currenttime > EndTime,!.

discover_new_optimal(Schedule, Cost,Finale_schedule,EndTime):-
    get_time(Currenttime),
    Currenttime < EndTime,
    generate_list_with_random_schedules(Schedule,List_with_random_changes),
    include(is_valid,List_with_random_changes,Result),
    not(is_empty(Result)),
    optimalcost_of_list(Result, New_optimal_cost,Resultoptimal),
    Cost > New_optimal_cost,
    discover_new_optimal(Resultoptimal,New_optimal_cost,Finale_schedule,EndTime),!.


discover_new_optimal(Schedule, Cost,Finale_schedule,EndTime):- 
    discover_new_optimal(Schedule, Cost,Finale_schedule,EndTime).

%% get_random_room(-RandomRID)
%% returns a random room
get_random_room(RandomRID):-
    findall(RID,room(RID,_),List_with_rooms),
    random_member(RandomRID,List_with_rooms),!.


%% make_random_event(+Event, -Random_event)
%% returns a random changed event
make_random_event(event(EID,_,_,_), event(EID,RandomRID, Day, Hour)):-
  duration(EID, Duration),
  get_random_room(RandomRID),
  findall(Days, availability(RandomRID,Days,_,_), AvailableDayList),
  random_permutation(AvailableDayList, RandomList),
  member(Day, RandomList),
  availability(RandomRID,Day,From,Till),
  MaxStartHour is Till - Duration,
  random_between(From, MaxStartHour, Hour).


%% optimalcost_of_list(+List_with_schedules,-Cost,-Schedule)
%% calculates the optimal cost and schedule from the given list with schedules
optimalcost_of_list([H|L],New_optimal_cost, New_optimal_schedule):-
    cost(H,CostX),
    optimalcost_of_list(L,Optimal_cost,Optimal_schedule),
    better(CostX, Optimal_cost, H, Optimal_schedule, New_optimal_schedule, New_optimal_cost).

optimalcost_of_list([],10^99,[]).


%% better(+Cost1,+Cost2,+Schedule1,+Schedule2,-Optimal_schedule,-Optimal_cost)
%% returns the best cost en schedule
better(CostX, Optimal_cost,ScheduleX, _,ScheduleX ,CostX):-
    CostX < Optimal_cost,!.

better(_, Optimal_cost,_, Optimal_schedule, Optimal_schedule ,Optimal_cost).


%% generate_list_with_random_schedules(+Valid_Schedule,-List)
%% returns a list with randomly changed schedules based on a valid schedule
generate_list_with_random_schedules(Valid_Schedule,List_with_random_changes):-
    generate_list_with_random_schedules(Valid_Schedule,25,List_with_random_changes).


generate_list_with_random_schedules(_,0,[]).

generate_list_with_random_schedules(Valid_Schedule,Acc,Resultlist):-
    Acc > 0,
    NewAcc is Acc - 1,
    generate_random_schedule(Valid_Schedule,Random_schedule),
    generate_list_with_random_schedules(Valid_Schedule,NewAcc, Result),
    append([Random_schedule],Result,Resultlist).

%% generate_random_schedule(+Schedule,-Random_schedule)
%% generates a random schedule
generate_random_schedule(schedule(EventList), schedule(UpdatedEventList)):-
    random_member(Random_chosen_event,EventList),
    delete(EventList,Random_chosen_event, Cleaned_eventList),
    make_random_event(Random_chosen_event,Random_changed_event),!,

    append(Cleaned_eventList,[Random_changed_event], UpdatedEventList).


