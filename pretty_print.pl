

%%  pretty_print(+S)
%%  Print out given schedule in humanreadable output
pretty_print(S):- 
    prepare_schedule(S,Prepared_data),
    write_schedule(Prepared_data).

 %% pretty_print(+List,-SortedList)
 %% sorteert gegeven lijst met evenementen
prepare_schedule(schedule([event(EID,RID,Day,Hour)| T]),Sortedlist_by_Hour_and_Date):-
    sort_list_by_hour([event(EID,RID,Day,Hour)| T], Sortedlist_by_Hour),
    sort_list_by_day(Sortedlist_by_Hour, Sortedlist_by_Hour_and_Date).


write_schedule([]).

write_schedule([event(EID,RID,Day,Hour)| T]):-

    get_events_with_same_day(Day,[event(EID,RID,Day,Hour)| T], Events_with_same_day),

    write('*** DAY '),write(Day),write(' ***'),
    nl,
    nl,
    write_small_rooms(Events_with_same_day),
    write_large_rooms(Events_with_same_day),
    nl,
    get_events_different_day(Day,[event(EID,RID,Day,Hour)| T], Events_with_different_day),
    write_schedule(Events_with_different_day).

%% is_small_room(+RID)
%% checks if room is big
is_small_room(RID):-
    room(RID,'Small room').

%% is_large_room(+RID)
%% checks if room is big
is_large_room(RID):-
    room(RID,'Large room').
    

write_small_rooms([]).
write_small_rooms([event(EID,RID,Day,Hour)| T]):-
    is_small_room(RID),
    write('Small Room'),write(':'),nl,
    write_event_info(event(EID,RID,Day,Hour)),
    write_small_rooms_without_title(T),
    nl.

write_small_rooms([event(_,_,_,_)| T]):-
    write_small_rooms(T).

write_small_rooms_without_title([]).

write_small_rooms_without_title([event(EID,RID,Day,Hour)| T]):-
    write_event_info(event(EID,RID,Day,Hour)),
    write_small_rooms_without_title(T).

write_large_rooms([]).

write_large_rooms([event(EID,RID,Day,Hour)| T]):-
    is_large_room(RID),
    write('Large Room'),write(':'),nl,
    write_event_info(event(EID,RID,Day,Hour)),
    write_large_rooms_without_title(T),
    nl.

write_large_rooms([event(_,_,_,_)| T]):-
    write_large_rooms(T).

write_large_rooms_without_title([]).

write_large_rooms_without_title([event(EID,RID,Day,Hour)| T]):-
    write_event_info(event(EID,RID,Day,Hour)),
    write_large_rooms_without_title(T).

%% write_event_info(+Event)
%% prints eventinfo
write_event_info(event(EID,_,_,Hour)):-
    duration(EID,Duration),
    has_exam(CID,EID),
    teaches(LID,CID),
    course(CID,Coursename),
    lecturer(LID,Lecturers_name),
    EndHour is Hour + Duration,
    write(Hour),write(':00'),write(' - '),write(EndHour),write(':00  '),
    write(Coursename),write(' ('),write(Lecturers_name),write(')').


%% pretty_print(+SID,+Schedule)
%% print schedule of the given student in human readable output
pretty_print(SID,schedule([event(EID,RID,Day,Hour)| T])):-
    calculate_associations_students([event(EID,RID,Day,Hour)| T],Students_associations),
    get_assoc(SID,Students_associations,List_with_events_of_person),
    prepare_schedule(schedule(List_with_events_of_person),Prepared_data),
    write_schedule(Prepared_data).




