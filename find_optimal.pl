:- [is_valid].
:- [cost].


find_optimal(_) :- 
    assert(best(nil,10^99)), 
    is_valid(X), 
    cost(X,ValueX), 
    update_best(X,ValueX),
    fail.

find_optimal(X) :- 
    best(X,_),
    retract(best(_,_)).

update_best(X,ValueX) :- 
    best(_,ValueBest), 
    ValueX < ValueBest, 
    !, 
    retract(best(_,_)),
    assert(best(X,ValueX)).
update_best(_,_).