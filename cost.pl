%% # Soft Constraints
%% # • Individual preferences:
%% #     – No exams over lunch break
%% #     – No exams in a (multi-hour/day) period
%% #     – Specific exam not in a (multi-hour/day) period
%% #     – No 2 exams on the same day.
%% #     – No 2 exams consectively. (BACK 2 BACK)
%% #     – Sufficient time to study for/correct all exams.


%
%-------- Utilitiy functions ------------%
%

%get_all_students_for_event(+Event,-List)
%returns a list with all the students following the event 
get_all_students_for_event(event(EID,_,_,_),Result_list):-
    has_exam(CID,EID),
    findall(SID,follows(SID,CID),Result_list).

%get_all_teachers_for_event(+Event,-List)
%returns a list with all the teachers of the event 
get_all_teachers_for_event(event(EID,_,_,_),Result_list):-
    has_exam(CID,EID),
    findall(LID,teaches(LID,CID),Result_list).

%get_list_with_students(-List)
%returns list with all students
get_list_with_students(List_with_students):-
    findall(X,student(X,_), List_with_students).


%get_list_with_teachers(-List)
%returns list with all teachers
get_list_with_teachers(List_with_teachers):-
    findall(X,lecturer(X,_), List_with_teachers).


%sort_list_by_hour(+List, -Sortedlist)
%sorts a list with events by hour in ascending order
sort_list_by_hour(List, Sortedlist):-
    sort(4,@=<,List,Sortedlist).


%sort_list_by_day(+List, -Sortedlist)
%sorts a list with events by day in ascending order
sort_list_by_day(List, Sortedlist):-
    sort(3,@=<,List,Sortedlist).


%insert_members_for_event(+Event,+List_with_persons,+Asscosiation, -New_Asscosiation)
%adds for every person in List_with_persons, the given Event as a value in the given Association 
%and returns the updated association.
insert_members_for_event(event(_,_,_,_),[],Old_associations,Old_associations).

insert_members_for_event(event(EID,RID,Day,Hour),[PID|T],Old_assoc,New_assoc):-
    insert_members_for_event(event(EID,RID,Day,Hour),T,Old_assoc,Assoc),
    get_assoc(PID,Assoc,Old_values),
    append([event(EID,RID,Day,Hour)],Old_values,New_values),
    put_assoc(PID,Assoc,New_values,New_assoc).

%initialize_assoc(+Assoc,+List_with_persons, -New_Assoc)
%initializes the given association with keys the persons 
%in the list and values the empty list
initialize_assoc(Old_Assoc,[],Old_Assoc).

initialize_assoc(Old_Assoc,[PID|L],Assoc_list):-
    initialize_assoc(Old_Assoc,L,Assoc),
    put_assoc(PID,Assoc,[],Assoc_list).


%% events_on_same_day(+Event,+Event)
%% checks wether two event are faliing on the same day
events_on_same_day(event(_,_,Day,_), event(_,_,Day2,_)):- 
    Day == Day2.

    
%% get_events_on_same_day(+Event,+List,-List)
%% returns a list with all the events on the same day as the given event
get_events_with_same_day(A,In,Out):-
    include(events_on_same_day(event(_,_,A,_)), In, Out).

%% get_events_different_day(+Event,+List,-List)
%% returns a list with all the events on a different day as the given event
get_events_different_day(A,In,Out):-
    exclude(events_on_same_day(event(_,_,A,_)), In, Out).


%
%-------- Top Level ---------------%
%

%% cost(+S,-Cost),
%% returns the cost of the given schedule
cost(Schedule, Cost):-
    %initialize hashmap
    calculate_penalties_students(Schedule, Student_cost),
    calculate_penalties_teachers(Schedule, Teacher_cost),!,
    sumall(Student_cost,Teacher_cost,Cost).

%                                  %
% ------- First Layer -------------%
%                                  %


%-- STUDENTS --%

%% calculate_penalties_students(+Schedule,-Student_cost)
%% calculates the total cost for the students
calculate_penalties_students(schedule([H|T]), Student_cost):-
    calculate_associations_students([H|T],Students_associations),
    penalties_students([H|T], Students_associations, Student_cost).



%-- TEACHERS --%

%% calculate_penalties_teachers(+Schedule,-Teachers_cost)
%% calculates the total cost for the teachers
calculate_penalties_teachers(schedule([H|T]), Teacher_cost):-
    calculate_associations_teachers([H|T],Teachers_associations),
    penalties_teachers([H|T], Teachers_associations, Teacher_cost).

%-- NORMALIZE --%


%% sumall(+Student_cost,+Teachers_cost, -Cost)
%% normalizes the sum of the Students_cost and Teachers_cost to the total cost
sumall(Student_cost, Teacher_cost,Cost):-
    aggregate_all(count, student(_,_), StudentCount),
    aggregate_all(count,  lecturer(_,_), TeacherCount),
    Student_count_norm is Student_cost / StudentCount,
    Teacher_count_norm is Teacher_cost / TeacherCount,
    Nominator is Student_count_norm + Teacher_count_norm,
    Cost is Nominator / 2 . 

%                                  %
% ------- Second Layer ------------%
%                                  %

%-- STUDENTS --%



%% calculate_associations_students(+List_with_event, -Assoc)
%% returns an associationlist populated with students and their participating courses
%% of the given list with events. Before poulating it sorts the given list in ascenending order.
calculate_associations_students(List,Students_associations):-
    sort_list_by_hour(List,Sorted_list_by_hour),
    sort_list_by_day(Sorted_list_by_hour,Sorted_list_by_day_and_hour),
    get_associations_students(Sorted_list_by_day_and_hour,Students_associations).

%% get_associations_students(+List_with_event, -Assoc)
%% returns an associationlist populated with students and their participating courses
%% of the given list with events
get_associations_students([], Assoc_list):-
    empty_assoc(Empty_assoc_list),
    %% %%initialize with all students bound to the empty_list
    get_list_with_students(List_with_students),
    initialize_assoc(Empty_assoc_list,List_with_students,Assoc_list).


get_associations_students([event(EID,RID,Day,Hour)|L],Final_students_associations):-
    get_all_students_for_event(event(EID,RID,Day,Hour),Students_of_event),
    get_associations_students(L,Old_associations),
    insert_members_for_event(event(EID,RID,Day,Hour),Students_of_event,Old_associations,Final_students_associations).

%% penalties_students(+List_with_event, +Assoc, -Penalty)
%% calculates the penalty for the students for the given list with events
penalties_students([H|T], Students_associations, Updated_penalty):-
     
    calc_penalties_with_no_interaction_for_students([H|T], Students_associations, Penalty_no_interaction),
    calc_penalties_with_interaction_for_students(Students_associations, Penalty_interaction),

    Updated_penalty is Penalty_no_interaction + Penalty_interaction.


%-- TEACHERS --%

%% calculate_associations_teachers(+List_with_event, -Assoc)
%% returns an associationlist populated with teacheres and their courses
%% of the given list with events. Before poulating it sorts the given list in ascenending order.
calculate_associations_teachers(List,Teachers_associations):-
    sort_list_by_hour(List,Sorted_list_by_hour),
    sort_list_by_day(Sorted_list_by_hour,Sorted_list_by_day_and_hour),
    get_associations_teachers(Sorted_list_by_day_and_hour,Teachers_associations),!.


%% get_associations_teachers(+List_with_event, -Assoc)
%% returns an associationlist populated with teachers and their participating courses
%% of the given list with events
get_associations_teachers([], Assoc_list):-
    empty_assoc(Empty_assoc_list),
    %% %%initialize with all students bound to the empty_list
    get_list_with_teachers(List_with_teachers),
    initialize_assoc(Empty_assoc_list,List_with_teachers,Assoc_list).


get_associations_teachers([event(EID,RID,Day,Hour)|L],Final_teachers_associations):-
    get_all_teachers_for_event(event(EID,RID,Day,Hour),Teachers_of_event),
    get_associations_teachers(L,Old_associations),
    insert_members_for_event(event(EID,RID,Day,Hour),Teachers_of_event,Old_associations,Final_teachers_associations).




%% penalties_teachers(+List_with_event, +Assoc, -Penalty)
%% calculates the penalty for the teachers for the given list with events
penalties_teachers([H|T], Teachers_associations, Updated_penalty):-
    calc_penalties_with_no_interaction_for_teachers([H|T], Teachers_associations, Penalty_no_interaction),
    calc_penalties_with_interaction_for_teachers(Teachers_associations, Penalty_interaction),
    Updated_penalty is Penalty_no_interaction + Penalty_interaction.


%                                  %
% ------- Third Layer -------------%
%                                  %


%-- STUDENTS --%

%calc_penalties_with_no_interaction_for_students(+List_with_students,+Assoc, -Penalty)
%calculates the penalty for the students where no interaction 
%with other events is needed for the penaltycalculation
calc_penalties_with_no_interaction_for_students([],_,0).

calc_penalties_with_no_interaction_for_students([H|T], Students_associations, Penalty_no_interaction):-
    get_all_students_for_event(H,List_with_students_of_event),

    penalties_lunch(H,List_with_students_of_event,Lunch_penalty),!,
    penalties_notspecexaminperiod(H,List_with_students_of_event,Notspecexaminperiod_penalty),

    calc_penalties_with_no_interaction_for_students(T,Students_associations,Penalty),
    
    Penalty_no_interaction is Penalty + Lunch_penalty + Notspecexaminperiod_penalty. 

%calc_penalties_with_interaction_for_students(+List_with_students,+Assoc, -Penalty)
%calculates the penalty for the students where interaction 
%with other events is needed for the penaltycalculation
calc_penalties_with_interaction_for_students(Students_associations,Penalty_interaction):-
    get_list_with_students(List_with_students),
    penalties_sameday(List_with_students,Students_associations,Sameday_penalty),
    
    penalties_b2b(List_with_students,Students_associations,B2b_penalty),
    penalties_notsufficienttime_to_study(List_with_students,Students_associations,Notsufficienttime_penalty),
    
    Penalty_interaction is Sameday_penalty + B2b_penalty + Notsufficienttime_penalty.


%-- Teachers --%

%calc_penalties_with_no_interaction_for_teachers(+List_with_teachers,+Assoc, -Penalty)
%calculates the penalty for the teachers where no interaction 
%with other events is needed for the penaltycalculation
calc_penalties_with_no_interaction_for_teachers([],_,0).
calc_penalties_with_no_interaction_for_teachers([H|T], Teachers_associations, Penalty_no_interaction):-
    get_all_teachers_for_event(H,List_with_teachers_of_event),
    
    penalties_lunch(H,List_with_teachers_of_event,Lunch_penalty),!,
    penalties_notspecexaminperiod(H,List_with_teachers_of_event,Notspecexaminperiod_penalty),
    penalties_noexaminperiod(H,List_with_teachers_of_event,Noexaminperiod_penalty),
    
    calc_penalties_with_no_interaction_for_teachers(T,Teachers_associations,Penalty),

    Penalty_no_interaction is Penalty + Lunch_penalty + Notspecexaminperiod_penalty + Noexaminperiod_penalty.


%calc_penalties_with_interaction_for_teachers(+List_with_teachers,+Assoc, -Penalty)
%calculates the penalty for the teachers where interaction 
%with other events is needed for the penaltycalculation
calc_penalties_with_interaction_for_teachers(Teachers_associations,Penalty_interaction):-
    get_list_with_teachers(List_with_teachers),

    penalties_sameday(List_with_teachers,Teachers_associations,Sameday_penalty),
    penalties_b2b(List_with_teachers,Teachers_associations,B2b_penalty),
    penalties_notsufficienttime_to_correct(List_with_teachers,Teachers_associations,Notsufficienttime_penalty),
    Penalty_interaction is Sameday_penalty + B2b_penalty + Notsufficienttime_penalty.




%% #
%% #     – No exams over lunch break
%% #

%% penalties_lunch(+Event,+List_with_Persons, -Penalty)
%% calculates the penalty of violating the soft constraint "No exams over lunch break" 
%% for the given event 
penalties_lunch(event(_,_,_,_),[],0). %no students, no penalty to give

penalties_lunch(event(EID,RID,Day,Hour),[PID|T],Sum_of_penalty):-
    is_during_lunch(EID,Hour), %%falls the event in lunchbreak
    sc_lunch_break(PID,New_penalty),
    penalties_lunch(event(EID,RID,Day,Hour),T,Penalty),
    Sum_of_penalty is Penalty + New_penalty,!.

penalties_lunch(event(_,_,_,_),[_|_],0).


%% is_during_lunch(+EID,+StartHour)
%% checks of the given event is during the lunchbreak
is_during_lunch(EID,StartHour):-
    duration(EID,Duration),
    Endhour is StartHour + Duration,
    StartHour =< 12,
    Endhour >= 13.



%% #
%% #     – Specific exam not in a (multi-hour/day) period
%% #

%% penalties_notspecexaminperiod(+Event,+List_with_Persons, -Penalty)
%% calculates the penalty of violating the soft constraint "Specific exam not in a (multi-hour/day) period" 
%% for the given event 
penalties_notspecexaminperiod(event(_,_,_,_),[],0).

penalties_notspecexaminperiod(event(EID,RID,Day,Hour),[PID|T], Notspecexaminperiod_penalty):-
    sc_not_in_period(PID,EID,Day,From,Till,New_penalty),
    is_overlap(EID,Hour,From,Till),
    penalties_notspecexaminperiod(event(EID,RID,Day,Hour),T,Penalty),
    Notspecexaminperiod_penalty is New_penalty + Penalty,!.

%event is in a valid periode ands no has to be given.
penalties_notspecexaminperiod(event(_,_,_,_),[_|_],0).

%% is_overlap(+EID,+StartHour,+From,+Till)
%%checks it event overlaps with given period.
is_overlap(EID,StartHour,From,Till):-
    duration(EID, Duration),
    EndHour is StartHour + Duration,
    StartHour < Till,
    EndHour > From.





%% #
%% #     – No exams in a (multi-hour/day) period
%% # 

%% penalties_noexaminperiod(+Event,+List_with_Persons, -Penalty)
%% calculates the penalty of violating the soft constraint " No exams in a (multi-hour/day) period" 
%% for the given event 
penalties_noexaminperiod(event(_,_,_,_),[],0).

penalties_noexaminperiod(event(EID,RID,Day,Hour),[PID|T],Noexaminperiod_penalty):-

    sc_no_exam_in_period(PID,Day,From,Till,New_Penalty),
    is_overlap(EID,Hour,From,Till),
    penalties_noexaminperiod(event(EID,RID,Day,Hour),T,Penalty),
    Noexaminperiod_penalty is Penalty + New_Penalty,!.

penalties_noexaminperiod(event(_,_,_,_),[_|_],0).


%% #
%% #     – No 2 exams on the same day.
%% #

%% penalties_sameday(+List_with_Persons,+Assoc, -Penalty)
%% calculates the total penalty of violating the soft constraint " No 2 exams on the same day." 
%% for the given list with persons 

penalties_sameday([],_,0).

penalties_sameday([PID|T],Associations,Sameday_penalty):-
    calculate_sameday_penalty(PID,Associations,New_penalty),
    penalties_sameday(T,Associations,Penalty),
    Sameday_penalty is Penalty + New_penalty.


%% calculate_sameday_penalty(+PID,+Associations,-Penalty)
%% calculates the total sameday person for a person
calculate_sameday_penalty(PID,Associations,Penalty):-
    get_assoc(PID,Associations,List_with_events_of_person),
    calculate_sameday_penalty_for_person(PID,List_with_events_of_person,Penalty),



%% possible_pairs(+List_with_events,-Possible_pairs)
%% returns the possible pairs of events
possible_pairs([],0).
possible_pairs([event(_,_,_,_)],0).

possible_pairs(List,Possible_pairs):-
    length(List,N),
    Second_part_nominator is N - 1,
    Nominator is N * Second_part_nominator,
    Possible_pairs is div(Nominator,2). % possible_pairs = n(n - 1)/2 




%% calculate_sameday_penalty_for_person(+PID,+List_with_events,-Penalty)
%% calculates the total sameday penalty for a given person with the given list of events
calculate_sameday_penalty_for_person(_,[],0).

calculate_sameday_penalty_for_person(PID,[event(EID,RID,Day,Hour)|T], Penalty_of_sameday_for_person):-
        get_events_with_same_day(Day,[event(EID,RID,Day,Hour)|T],Events_with_same_day),
        possible_pairs(Events_with_same_day, Possible_pairs),
        get_same_day_penalty(PID,Penalty),
        New_penalty is Possible_pairs * Penalty,
        get_events_different_day(Day,[event(EID,RID,Day,Hour)|T],Events_with_different_day),
        calculate_sameday_penalty_for_person(PID,Events_with_different_day, Other_Penalty),
        Penalty_of_sameday_for_person is New_penalty + Other_Penalty,!.

%% get_same_day_penalty(+PID,-Penalty)
%% calculates the sameday penalty for the given PID
get_same_day_penalty(PID,Penalty):-
    sc_same_day(PID,Penalty),!.

get_same_day_penalty(_,0).


%% #
%% #     – No 2 exams consectively (BACK 2 BACK).
%% #


%% penalties_b2b(+List_with_Persons,+Associations, -Penalty)
%% calculates the total penalty of violating the soft constraint "  No 2 exams consectively (BACK 2 BACK)." 
%% for the given list  with persons

penalties_b2b([],_,0).

penalties_b2b([PID|T],Associations,B2b_penalty):-
    calculate_b2b_penalty_for_person(PID,Associations,New_penalty),
    penalties_b2b(T,Associations,Penalty),
    B2b_penalty is Penalty + New_penalty.


calculate_b2b_penalty_for_person(PID,Associations,Penalty):-
    get_assoc(PID,Associations,List_with_events_of_person),
    pairwise_b2b_comparison(PID,List_with_events_of_person, Penalty).



%% pairwise_b2b_comparison(+PID,+List_with_events, -Penalty)
%% compares pairwise the events in the list and checks if they are consective.
%% if so a penalty is initiated. At the end it returns the total sum of the penalties

pairwise_b2b_comparison(_,[],0).
%events are in order! the earliest event is first in the list
pairwise_b2b_comparison(PID,[event(EID,RID,Day,Hour)|T],Total_penalty):-
    get_events_with_same_day(Day,T,Events_on_same_day),
    is_consectively(PID,event(EID,RID,Day,Hour),Events_on_same_day,New_penalty),
    pairwise_b2b_comparison(PID,T,Penalty),
    Total_penalty is Penalty + New_penalty.


%% is_consectively(+PID,+Event, +List_with_events, -Penalty)
%% checks the given event with every event in the list and return the total penalty

is_consectively(_,event(_,_,_,_),[],0).

is_consectively(PID,event(EID,_,_,Hour),[event(_,_,_,Hour2)|_],New_penalty):-
    duration(EID,DUR),
    Diff is Hour2 - Hour,
    DUR == Diff,
    sc_b2b(PID,New_penalty),!.

%als het niet consectively, is de penalty 0
is_consectively(_,event(_,_,_,_),[event(_,_,_,_)|_],0).


%% #
%% #     – Sufficient time to study for/correct all exams.
%% #


%---- Student ---- %

%% penalties_notsufficienttime_to_study(+List_with_Persons,+Assoc, -Penalty)
%% calculates the total penalty of violating the soft constraint "Sufficient time to study for/correct all exams." 
%% for the given list with persons 

penalties_notsufficienttime_to_study([],_,0).


penalties_notsufficienttime_to_study([PID|T],Associations,Notsufficienttime_penalty):-
    first_day(Firstday),
    last_day(Lastday),
    get_assoc(PID,Associations,List_with_events_of_person),
    reverse(List_with_events_of_person,Reversed_list_with_events_of_person),
    calc_notsufficienttime_to_study_penalty_for_person(PID, Reversed_list_with_events_of_person, Firstday, Lastday,Penalty_of_person),
    penalties_notsufficienttime_to_study(T,Associations,Penalty),
    Notsufficienttime_penalty is Penalty + Penalty_of_person.    

%% penalties_notsufficienttime_to_study_penalty_for_person(+PID,+List_with_events, -Penalty)
%% calculates the total penalty of violating the soft constraint "Sufficient time to study for/correct all exams." 
%% for the given PID

calc_notsufficienttime_to_study_penalty_for_person(PID, [event(EID,RID,Day,Hour)|T], Firstday,_,Penalty_of_person):-
    calc_notsufficienttime_to_study_with_pointer([event(EID,RID,Day,Hour)|T],Day,Begin_day_of_study),
    Firstday > Begin_day_of_study,
    Multiplier_of_penalty is Firstday - Begin_day_of_study,
    sc_study_penalty(PID,Penalty),
    Penalty_of_person is Penalty * Multiplier_of_penalty,!.

calc_notsufficienttime_to_study_penalty_for_person(_,[],_,_,0):- !.

calc_notsufficienttime_to_study_penalty_for_person(_,[event(_,_,_,_)|_], _,_,0).


%% penalties_notsufficienttime_with_pointer(+List_with_events, +Pointer,-Final_Pointer)
%% calculates the days needed to study

calc_notsufficienttime_to_study_with_pointer([],Pointer, Pointer).

calc_notsufficienttime_to_study_with_pointer([event(EID,RID,Day,Hour)|T],Pointer,Final_position):-
    calc_new_pointer_to_study(event(EID,RID,Day,Hour),Pointer,New_Pointer),
    calc_notsufficienttime_to_study_with_pointer(T,New_Pointer,Final_position).


%% calc_new_pointer_to_study(+Event, +Pointer,-Final_Pointer)
%% calculates the days needed to study for the given event

calc_new_pointer_to_study(event(EID,_,Day,_), Old_Pointer, New_Pointer):-
    Old_Pointer =< Day, %Als examen al afgelegd afgelegd moet worden na study_deadline
    sc_study_time(EID,Studytime),
    New_Pointer is Old_Pointer - Studytime,!.

calc_new_pointer_to_study(event(EID,_,Day,_), _, New_Pointer):-
    sc_study_time(EID,Studytime),
    New_Pointer is Day - Studytime.



%--- Teacher ----%

%% penalties_notsufficienttime_to_correct(+List_with_Persons,+Assoc, -Penalty)
%% calculates the total penalty of violating the soft constraint "Sufficient time to study for/correct all exams." 
%% for the given list with persons 

penalties_notsufficienttime_to_correct([],_,0).

penalties_notsufficienttime_to_correct([PID|T],Associations,Notsufficienttime_penalty):-
    first_day(Firstday),
    last_day(Lastday),
    get_assoc(PID,Associations,List_with_events_of_person),

    calc_notsufficienttime_to_correct_penalty_for_person(PID, List_with_events_of_person, Firstday, Lastday,Penalty_of_person),
    penalties_notsufficienttime_to_correct(T,Associations,Penalty),
    Notsufficienttime_penalty is Penalty + Penalty_of_person.    


%% penalties_notsufficienttime_to_correct_penalty_for_person(+PID,+List_with_events, -Penalty)
%% calculates the total penalty of violating the soft constraint "Sufficient time to study for/correct all exams." 
%% for the given PID

calc_notsufficienttime_to_correct_penalty_for_person(_,[],_,_,0):-!.

calc_notsufficienttime_to_correct_penalty_for_person(PID, [event(EID,RID,Day,Hour)|T], _,Lastday,Penalty_of_person):-
    calc_notsufficienttime_to_correct_with_pointer([event(EID,RID,Day,Hour)|T],Day,End_day_of_correct),
    Lastday < End_day_of_correct,
    Multiplier_of_penalty is End_day_of_correct - Lastday,
    sc_correction_penalty(PID,Penalty),
    Penalty_of_person is Penalty * Multiplier_of_penalty,!.


calc_notsufficienttime_to_correct_penalty_for_person(_,[event(_,_,_,_)|_], _,_,0).

%% penalties_notsufficienttime_to_correct_with_pointer(+List_with_events, +Pointer,-Final_Pointer)
%% calculates the days needed to correct
calc_notsufficienttime_to_correct_with_pointer([],Pointer, Pointer).

calc_notsufficienttime_to_correct_with_pointer([event(EID,RID,Day,Hour)|T],Pointer,Final_position):-
    calc_new_pointer_to_correct(event(EID,RID,Day,Hour),Pointer,New_Pointer),
    calc_notsufficienttime_to_correct_with_pointer(T,New_Pointer,Final_position).


%% calc_new_pointer_to_correct(+Event, +Pointer,-Final_Pointer)
%% calculates the days needed to correct for the given event

calc_new_pointer_to_correct(event(EID,_,Day,_), Old_Pointer, New_Pointer):-
    Old_Pointer >= Day, %Als examen al afgelegd is voor dat het vorige verbeterd is
    sc_correction_time(EID,Correctiontime),
    New_Pointer is Old_Pointer + Correctiontime,!.

calc_new_pointer_to_correct(event(EID,_,Day,_), _, New_Pointer):-
    sc_correction_time(EID,Correctiontime),
    New_Pointer is Day + Correctiontime.


