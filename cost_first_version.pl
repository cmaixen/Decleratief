%% # Soft Constraints
%% # • Individual preferences:
%% #     – No exams over lunch break
%% #     – No exams in a (multi-hour/day) period
%% #     – Specific exam not in a (multi-hour/day) period
%% #     – No 2 exams on the same day.
%% #     – No 2 exams consectively. (BACK 2 BACK)
%% #     – Sufficient time to study for/correct all exams.



%% Where Cost is the cost of valid schedule S.
%% The cost of a schedule is determined as the total sum of penalties of violated soft­constraints. 
%% As students typically greatly outnumber lecturers, penalties are normalized as follows: 
%% (penalties_lecturers/#lecturers + penalties_students/#students)/2
%% Here penalties_lecturers/students is the sum of penalties for constraints of all lecturers/students
%% resp. The first argument of a soft constraint predicate indicates the 'PID'
%% of the person (lecturer/student) having this constraint.

cost(Schedule, Cost):-
    calculate_penalties_students(Schedule,Student_cost).

cost(Schedule, Cost):-
    calculate_penalties_students(Schedule,Student_cost),
    calculate_penalties_teachers(Schedule,Teacher_cost),
    sumall(Student_cost,Teacher_cost,Cost ).


sumall(Student_cost, Teacher_cost,Cost):-
    aggregate_all(count, student(X,Y), StudentCount),
    aggregate_all(count,  teaches(P,Z), TeacherCount),
    Student_count_norm is div(Student_cost,StudentCount),
    Teacher_count_norm is div(Teacher_cost,TeacherCount),
    Nominator is Student_count_norm + Teacher_count_norm,
    Cost is div(Nominator,2). 


calculate_penalties_teachers(schedule([H|T]), Teacher_cost):-
    penalties_teachers([H|T],Teacher_cost).

penalties_teachers([],0).
penalties_teachers([H|T], Penalty + Lunch_penalty):-
    %%does not require  interaction with other events
    penalties_lunch(H,Lunch_penalty),
    penalties_notspecexaminperiod(H,Notspecexaminperiod_penalty),
    penalties_noexaminperiod(H,Noexaminperiod_penalty),
    %%does require interaction with other events
    %% penalties_notsufficienttime(H, Notsufficienttime_penalty),
    %% penalties_b2b(H,T,B2b_penalty),
    %% penalties_sameday(H,T,Sameday_penalty),
    %% penalties_sufficient_time(),
    penalties_teachers(T, Penalty).


%% penalties_teachers([H|T], Penalty + Lunch_penalty + B2b_penalty + Sameday_penalty + Notspecexaminperiod_penalty + Noexaminperiod_penalty):-
%%     %%does not require  interaction with other events
%%     penalties_lunch(H,Lunch_penalty),
%%     penalties_notspecexaminperiod(H,Notspecexaminperiod_penalty),
%%     penalties_noexaminperiod(H,Noexaminperiod_penalty),
%%     %%does require interaction with other events
%%     penalties_notsufficienttime(H, Notsufficienttime_penalty),
%%     penalties_b2b(H,T,B2b_penalty),
%%     penalties_sameday(H,T,Sameday_penalty),
%%     penalties_sufficient_time(),
%%     penalties_teachers(T, Penalty).


calculate_penalties_students(schedule([H|T]), Student_cost):-
    penalties_students([H|T], Student_cost).

penalties_students([],0).

penalties_students([H|T], Penalty + Lunch_penalty):-
    %%does not require  interaction with other events
    penalties_lunch(H,Lunch_penalty),
    %% penalties_notspecexaminperiod(H,Notspecexaminperiod_penalty),
    %% %%does require interaction with other events
    %% penalties_b2b(H,T,B2b_penalty),
    %% penalties_sameday(H,T,Sameday_penalty),
    %% penalties_notsufficienttime(H, Notsufficienttime_penalty),
    penalties_students(T,Penalty).

%% penalties_students([H|T], Penalty + Lunch_penalty + B2b_penalty + Sameday_penalty + Notspecexaminperiod_penalty):-
%%     %%does not require  interaction with other events
%%     penalties_lunch(H,Lunch_penalty),
%%     penalties_notspecexaminperiod(H,Notspecexaminperiod_penalty),
%%     %%does require interaction with other events
%%     penalties_b2b(H,T,B2b_penalty),
%%     penalties_sameday(H,T,Sameday_penalty),
%%     penalties_notsufficienttime(H, Notsufficienttime_penalty),
%%     penalties_students(T,Penalty).



%% #
%% #     – No exams over lunch break
%% #

penalties_lunch(event(EID,RID,Day,Hour),Sum_of_penalty):-
    12 =< Hour,
    13 >= Hour, %%falls the event in lunchbreak
    has_exam(CID,EID),

    aggregate_all(count, follows(SID,CID),Count),
    sc_lunch_break(SID,Penalty),
    Sum_of_penalty is Penalty * Count. 

penalties_lunch(event(EID,RID,Day,Hour),Sum_of_penalty):-
    12 =< Hour,
    13 >= Hour, %%falls the event in lunchbreak
    has_exam(CID,EID),
    teaches(LID,CID),
    aggregate_all(count,teaches(LID,CID),Count),
    sc_lunch_break(LID,Penalty),
    Sum_of_penalty is Penalty * Count. 

penalties_lunch(event(EID,RID,Day,Hour),0).

%% #
%% #     – Specific exam not in a (multi-hour/day) period
%% #

penalties_notspecexaminperiod(event(EID,RID,Day,Hour), Notspecexaminperiod_penalty):-
    has_exam(CID,EID),
    follows(SID,CID),
    sc_not_in_period(SID,EID,Day,From,Till,Penalty),
    in_valid_period(Hour,From,Till),
    aggregate_all(count,sc_not_in_period(SID,EID,Day,From,Till,Penalty),Counter),
    Notspecexaminperiod_penalty is Counter * Penalty.   

%%Als bovenstaande faalt is er geen penalty
penalties_notspecexaminperiod(event(EID,RID,Day,Hour), 0).

%%kijkt of het gegeven uur in de gespecifieerde periode ligt
in_valid_period(Hour,From,Till):-
    From =< Hour,
    Till >= Hour.





%% #
%% #     – No exams in a (multi-hour/day) period
%% # 

penalties_noexaminperiod(event(EID,RID,Day,Hour),Noexaminperiod_penalty):-
    has_exam(CID,EID),
    teaches(LID,CID),
    sc_no_exam_in_period(LID,Day,From,Till,Penalty),
    is_valid(Hour,From,Till),
    aggregate_all(count,sc_no_exam_in_period(LID,Day,From,Till,Penalty),Counter),
    Noexaminperiod_penalty is Penalty * Counter.

penalties_noexaminperiod(event(EID,RID,Day,Hour),0).


%% #
%% #     – No 2 exams on the same day.
%% #

events_on_same_day(event(_,_,Day,_), event(_,_,Day2,_)):- 
    Day == Day2.
%% %%return a list with all event on the same day
get_events_with_same_day(A,In,Out):-
    include(events_on_same_day(event(_,_,A,_)), In, Out).


%% Werking:
%% 1) gaat eerst alle examens verzamelen met dezelfde dag als meegegeven examen
%% 2) vergelijkt ieder examen van gefilterde lijst met het meegegeven examen en berekend de penalty
penalties_sameday(event(EID,RID,Day,Hour),[],0).

penalties_sameday(event(EID,RID,Day,Hour), Other_events, Sameday_penalty):-
    get_events_with_same_day(Day, Other_events , Events_on_same_day),
    calc_sameday_penalty_for_exam(event(EID,RID,Day,Hour),Events_on_same_day,Sameday_penalty).


calc_sameday_penalty_for_exam(event(EID,RID,Day,Hour),[],0). %%there are no events with the same day
calc_sameday_penalty_for_exam(event(EID,RID,Day,Hour),[event(EID2,RID2,Day,Hour2)|T], Penalty + Sameday_penalty):-
    get_total_sameday_penalty(EID,EID2,Sameday_penalty),
    calc_sameday_penalty_for_exam(event(EID,RID,Day,Hour),T,Penalty).

%%penalty when student
get_total_sameday_penalty(EID,EID2,Sameday_penalty):-
    has_exam(CID,EID),
    has_exam(CID2,EID2),
    follows(SID, CID2),
    follows(SID,CID),
    aggregate_all(count,sc_same_day(SID,Penalty), Counter),
    Sameday_penalty is Counter * Penalty.

%%penalty when teacher
get_total_sameday_penalty(EID,EID2,Sameday_penalty):-
    has_exam(CID,EID),
    has_exam(CID2,EID2),
    teaches(LID, CID2),
    teaches(LID,CID),
    aggregate_all(count,sc_same_day(LID,Penalty), Counter),
    Sameday_penalty is Counter * Penalty.

%% #
%% #     – No 2 exams consectively (BACK 2 BACK).
%% #

%% back2back is total different approach
%% 1) verzamelen alle dagen 
%% 2) sorteer de dagen
%% 3) loop alle dagen af en filter telkens op dag
%% 4) en tel zo de penality

sort_list_by_hour(List, Sortedlist):-
    sort(4,@=<,List,Sortedlist).

sort_list_by_day(List, Sortedlist):-
    sort(3,@=<,List,Sortedlist).


%event2 start wanneer event1 eindigt
is_consectively(EID,HOUR,EID2,Hour2):-
    duration(EID,DUR),
    Diff is abs(Hour - Hour2),
    DUR == Diff.

%event2 eindigt wanneer event1 start.
is_consectively(EID,HOUR,EID2,Hour2):-
    duration(EID2,DUR2),
    Diff is abs(Hour - Hour2),
    DUR2 == Diff.



penalties_b2b(event(EID,RID,Day,Hour),[],0).

penalties_b2b(event(EID,RID,Day,Hour), Other_events, B2b_penalty):-
    get_events_with_same_day(Day, Other_events , Events_on_same_day),
    calc_total_b2b_penalty_for_exam(event(EID,RID,Day,Hour),Events_on_same_day,B2b_penalty).

calc_total_b2b_penalty_for_exam(event(EID,RID,Day,Hour),[],0).

calc_total_b2b_penalty_for_exam(event(EID,RID,Day,Hour),[event(EID2,RID2,Day2,Hour2)|T],B2b_penalty + Penalty):-
        get_b2b_penalty(EID,HOUR,EID2,Hour2, B2b_penalty),
        calc_total_b2b_penalty_for_exam(event(EID,RID,Day,Hour),T,Penality).


get_b2b_penalty(EID,HOUR,EID2,Hour2,B2b_penality):-
    is_consectively(EID,HOUR,EID2,Hour2),
    has_exam(CID,EID),
    has_exam(CID2,EID2),
    follows(SID, CID2),
    follows(SID,CID),
    aggregate_all(count,sc_b2b_day(SID,Penalty), Counter),
    B2b_penalty is Counter * Penality.

get_b2b_penalty(EID,HOUR,EID2,Hour2,B2b_penality):-
    is_consectively(EID,HOUR,EID2,Hour2),
    has_exam(CID,EID),
    has_exam(CID2,EID2),
    teaches(SID, CID2),
    teaches(SID,CID),
    aggregate_all(count,sc_b2b_day(SID,Penalty), Counter),
    B2b_penalty is Counter * Penality.





%% #
%% #     – Sufficient time to study for/correct all exams.
%% #


penalties_notsufficienttime(event(EID,RID,Day,Hour), Notsufficienttime_penalty).
    

penalties_notsufficienttime(event(EID,RID,Day,Hour), 0).







    
