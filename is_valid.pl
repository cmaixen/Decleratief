

%% Hard Constraints
%% • All exams must be scheduled exactly once.
%% • Exams can only take place in a room...
%%      – that is available for the entire period of the exam
%%      – whose capacity exceeds or equals the number of students attending the exam (subscribed to the course).
%% • No 2 exams can take place at the same time...
%%      – in the same room
%%      – if 1 or more students are subscribed to both courses (this includes multiple exams of the same course).
%%      – if the same lecturer teaches both courses



%% not_membercheck(+EID,+List_with_events)
%% checks if event is not a member of the list 
not_membercheck(_,[]).
not_membercheck(EID,[event(EID2,_,_,_)|T]):-
    EID \= EID2,
    not_membercheck(EID, T).


%% checking if the given event is valid = heeft een bestaande EID en RID:

%% is_valid(?S)
%% generates a valid schedule and can validate a schedule
is_valid(schedule(List_with_events)):-
    findall(X,exam(X,_), Amount_of_events), %list of all the required events
    is_valid(List_with_events,Amount_of_events,[]).


is_valid([],[],_). % OK if given_schedule and Amount_of_events are from the same lenght
is_valid([event(EID,RID,Day,Hour)|T], [_|T2], Verify_list ):-
        is_valid_event(event(EID,RID,Day,Hour)),
        not_membercheck(EID,Verify_list),!,
        is_room_valid(event(EID,RID,Day,Hour)),
        is_valid_period_and_time(event(EID,RID,Day,Hour)),
        is_valid_exam(event(EID,RID,Day,Hour),Verify_list),
       %checks if event is seen for the first time 
        append(Verify_list,[event(EID,RID,Day,Hour)], Verify_list_updated),

        is_valid(T,T2, Verify_list_updated).



%% is_valid_event(+Event)
%% checks if event is valid
is_valid_event(event(EID,_,_,_)) :-
    exam(EID,_).


%% --- HARD CONSTRAINTS ---

%% is_valid_period_and_time(+Event)
%% Checks if given event is falling in the valid period
is_valid_period_and_time(event(_,_,Day,Hour)):-
    first_day(First),
    last_day(Last), 
    not(First > Day),
    not(Day > Last),
    integer(Hour).




%% Exams can only take place in a room:
%%     1) that is available for the entire period of the exam
%%     2) whose capacity exceeds or equals the number of students attending the exam (subscribed to the course).



%% is_room_valid(+Event)
%% check if room is valid
is_room_valid(Event):-
        is_room_available(Event),
        is_room_big_enough(Event).

%% is_room_available(+Event)
%% checks if room is available
is_room_available(event(EID,RID,Day,Hour)):-
        availability(RID,Day,From,Till),
        duration(EID,DUR),

        valid_time(Hour,From,Till),
        is_correct_hour(Hour),
        From =< Hour,
        Hour + DUR =< Till.

%% is_correct_hour(+Hour)
%% checks if hour is correct
valid_time(Time,From,Till) :-
  var(Time),
  between(From,Till,Time).

valid_time(Time,_,_) :-
  nonvar(Time),
  integer(Time),
  between(0,24,Time).



is_correct_hour(Hour):-
    integer(Hour),
    Hour >= 0,
    Hour =< 24.

%% is_overlapping_exam_in_room(+Event,+Event)
%% checks if event is overlapping in a room with another event
is_overlapping_exam_in_room(event(EID,RID,Day,Hour),event(_,RID2,Day2,Hour2)):-
        RID = RID2,
        Day = Day2,
        duration(EID,DUR),
        abs(Hour - Hour2) < DUR.

%% is_room_big_enough(+Event)
%% checks if room of event is big enough
is_room_big_enough(event(EID,RID,_,_)):-
        has_exam(CID,EID),
        aggregate_all(count, follows(_,CID), Count),
        capacity(RID,Capacity),
        Capacity >= Count.


%% • No 2 exams can take place at the same time...
%%      – in the same room
%%      – if 1 or more students are subscribed to both courses (this includes multiple exams of the same course).
%%      – if the same lecturer teaches both courses

%% are_exams_valid(+List_with_events)
%% checks if given list with events is valid
are_exams_valid([]).
are_exams_valid([event(EID,RID,Day,Hour)|T]):-
    is_valid_exam(event(EID,RID,Day,Hour),T),
    are_exams_valid(T).

%% is_valid_exam(+Event)
%% checks if given Event is valid
is_valid_exam(event(_,_,_,_),[]).
is_valid_exam(event(EID,RID,Day,Hour),[event(EID2,RID2,Day2,Hour2)|T]):-
    not(is_overlapping_exam_in_room(event(EID,RID,Day,Hour),event(EID2,RID2,Day2,Hour2))),
    not(is_overlapping_exam_for_students(event(EID,RID,Day,Hour),event(EID2,RID2,Day2,Hour2))),
    not(is_overlapping_exam_for_teachers(event(EID,RID,Day,Hour),event(EID2,RID2,Day2,Hour2))),
    is_valid_exam(event(EID,RID,Day,Hour),T).


%% is_overlapping_event_in_time(+Event,+Event)
%% checks if given two events are overlapping in time
is_overlapping_event_in_time(event(EID,_,Day,Hour),event(_,_,Day2,Hour2)):-
        Day = Day2,
        duration(EID,DUR),
        abs(Hour - Hour2) < DUR.

%% is_overlapping_exam_for_students(+Event,+Event)
%% checks if given two events are overlapping in time for students
is_overlapping_exam_for_students(event(EID,RID,Day,Hour),event(EID2,RID2,Day2,Hour2)):-
        is_overlapping_event_in_time(event(EID,RID,Day,Hour),event(EID2,RID2,Day2,Hour2)),
        has_exam(CID,EID),
        has_exam(CID2,EID2),
        follows(StudentID,CID),
        follows(StudentID,CID2). %from the moment 1 student has a collision there is a problem

%% is_overlapping_exam_for_teachers(+Event,+Event)
%% checks if given two events are overlapping in time for teachers.      
is_overlapping_exam_for_teachers(event(EID,RID,Day,Hour),event(EID2,RID2,Day2,Hour2)):-
        is_overlapping_event_in_time(event(EID,RID,Day,Hour),event(EID2,RID2,Day2,Hour2)),
        has_exam(CID,EID),
        has_exam(CID2,EID2),
        teaches(TeachersID,CID),
        teaches(TeachersID,CID2).








